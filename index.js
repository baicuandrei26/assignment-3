
const FIRST_NAME = "Andrei";
const LAST_NAME = "Baicu";
const GRUPA = "1075";

/**
 * Make the implementation here
 */
class Employee {

    constructor(name, surname, salary)
    {
        this.name = name;
        this.surname = surname;
        this.salary = salary;
    }

    getDetails() {
     return this.name + " " + this.surname + " " + this.salary;  
    }
}

class SoftwareEngineer extends Employee{
   
    constructor(name, surname, salary, experience)
   {
       super(name, surname, salary);
       if(experience === undefined){
           this.experience = "JUNIOR";
       }else{
            this.experience = experience;
       }
   }

   applyBonus() {
       switch (this.experience) {
           case "JUNIOR":
              return this.salary + this.salary*0.1;
               
            case "MIDDLE":
               return  this.salary + this.salary*0.15;
            case"SENIOR":
                return this.salary + this.salary*0.2;
            default:
                return this.salary + this.salary*0.1;
       }
   }
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}

